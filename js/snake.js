(function(window){
	//Setup the Snake Object
	Snake = function(map,snake){
		var i;
		
		this.queue = snake || [];
		this.map = map || [[]];

		this.collisions = [];
		this.direction = 2;
		this.running = true;
		this.allowGrow = false;
		
		for(i=0;i<this.queue.length;i+=1){
			this._render(this.queue[i],1);
		}
		
	};
	
	Snake.prototype = {
		_render : function(pos,value){
			if(
				this.map[pos.x] instanceof Array &&
				this.map[pos.x][pos.y] !== undefined
			){
				this.map[pos.x][pos.y] = value || 0;
			}
			
		},
		_prepend : function(part){
			var start = this.queue[0];
			
			this.queue.push(part);
		},
		_remove : function(ammount){
			var q = this.queue,
				end = q[q.length-1];

			if(this.allowGrow === false){
				return this.queue.splice(end,1);
			}

			this.allowGrow = false;

		},
		changeDir : function(direction){
			if(
				this.direction === 0  && direction !== 2 ||
				this.direction === 2  && direction !== 0 ||
				this.direction === 1  && direction !== 3 ||
				this.direction === 3  && direction !== 1 

			){
				this.direction = direction;
			}
			
		},
		stop : function(){
			this.running = false;
		},
		grow : function(){
			this.allowGrow = true;
		},
		addCollision : function(value,callback){
			this.collisions.push({"value":value,"callback":callback});
		},
		checkCollision : function(params){

			var x = params.x,
				y = params.y,
				cols = this.collisions;

			for(var i=0;i<cols.length;i+=1){
				if(cols[i].value === this.map[x][y]){
					cols[i].callback();
				}
			}

		},
		forward : function(){
			var len = this.queue.length,
				that = this,
				i,x,y,
				end = this.queue[len-1];
			
			if(this.running){

				switch(this.direction){
					//Left
					case 0:
						x=end.x-1;
						y=end.y;
					break;
					//Right
					case 2:
						x=end.x+1;
						y=end.y;
					break;
					//Up
					case 1:
						x=end.x;
						y=end.y-1;
					break;
					//Down
					case 3:
						x=end.x;
						y=end.y+1;
					break;
					default:
						break;
				}

				this.checkCollision({x:x,y:y});

				if(this.running){

					for(i=0;i<len;i+=1){
						that._render(that.queue[i],0);
					}

					that._remove(1);

					that._prepend({x:x,y:y});

					for(i=0;i<len;i+=1){
						that._render(that.queue[i],1);
					}

				}

			}
		}
	}

	window.Snake = Snake;

}(this));