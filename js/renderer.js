(function(){
	//Global Vars
	var i,j,	
		//Canvas
		canvas,ctx,
		//Measurements
		width,height,cellSize,
		//System vars
		map=[],mapWidth,mapHeight,score=0,
		//Global Methods
		draw,drawMap,dropFood;
	
	//Get the canvas and the context
	canvas = document.getElementById('myCanvas');
	ctx = canvas.getContext('2d');
	
	//Get the width and height of the scene
	width = canvas.width;
	height = canvas.height;
	
	//Set the size of a cell
	cellSize = 15;
	
	//Set the map array width and height;
	mapWidth  = width/cellSize;
	mapHeight = height/cellSize;
	
	//Setup the map
	for(i=0;i<mapWidth;i+=1){
		map[i] = [];
		for(j=0;j<mapHeight;j+=1){
			if(
				//Make a wall of collision
				i === 0 ||
				i === (mapWidth - 1) ||
				j === 0 ||
				j === (mapHeight - 1)
			){
				map[i][j] = 3;
			}else{
				map[i][j] = 0;
			}
			
		}
	}

	//Generate a food pellet
	dropFood = function(map,ammnt){
		var x,y,
			xLength = map.length - 2,
			yLength = map[0].length - 2;

		x = Math.floor(1 + (1+xLength-1)*Math.random());
		y = Math.floor(1 + (1+yLength-1)*Math.random());

		if(map[x][y] === 0){
			map[x][y] = 2;
		}else{
			dropFood(map,ammnt);
		}
		
	}

	//Render Map
	drawMap = function(map){
		var i,j,x,y;
		for(i=0;i<map.length;i+=1){
			for(j=0;j<map[i].length;j+=1){
				x = i*cellSize;
				y = j*cellSize;
				
				ctx.fillStyle = "rgb(200,200,200)";
				ctx.strokeStyle = "rgb(0,0,0)";  
					
				if(map[i][j] === 1){
					ctx.fillStyle = "rgb(10,100,10)";
					ctx.strokeStyle = "rgb(0,0,0)";
				}

				if(map[i][j] === 2){
					ctx.fillStyle = "rgb(150,10,10)";
					ctx.strokeStyle = "rgb(0,0,0)";
				}

				if(map[i][j] === 3){
					ctx.fillStyle = "rgb(50,50,50)";
					ctx.strokeStyle = "rgb(0,0,0)";
				}
				
				ctx.fillRect (x, y, x+cellSize, y+cellSize);
				//ctx.strokeRect(x, y, x+cellSize, y+cellSize);
			}
		}
	};
	
	var click = 0;

	//Draw on the Screen
	draw = function(){
		
		if(click === 3){
			//Move the snake forward
			mySnake.forward();
			click = 0;
		}

		click += 1;

		//Clear the Screen
		ctx.fillStyle = "rgb(255,255,255)";
		ctx.fillRect(0,0,width,height);

		//Redraw the Map
		drawMap(map);

	};

	dropFood(map,1);

	//Create my New Snake Object
	mySnake = new Snake(map,[{x:1,y:5},{x:2,y:5},{x:3,y:5},{x:4,y:5},{x:5,y:5},{x:6,y:5},{x:7,y:5}]);
	
	//Add some snake collisions
	mySnake.addCollision(1,function(){
		console.log("Collided with itself");
		mySnake.stop();
	});

	mySnake.addCollision(3,function(){
		console.log("Snake hit a wall");
		mySnake.stop();
	});

	mySnake.addCollision(2,function(){
		console.log("ate some food");
		score += 10;
		$("#score").html(score);
		mySnake.grow();
		dropFood(map,1);
	});

	//Animation Loop
	(function tik(){
		requestAnimFrame(tik);
		draw();
	})();

	//Snake Controls
	$(window).keydown(function(event) {

		switch(event.keyCode){
			case 37:
				mySnake.changeDir(0);
				break;
			case 38:
				mySnake.changeDir(1);
				break;
			case 39:
				mySnake.changeDir(2);
				break;
			case 40:
				mySnake.changeDir(3);
				break;
		}

	});
	
	
}());